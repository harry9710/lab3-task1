/*Laboratory 3
	+ write a program that takes as input first: number of names
	+ than reads these names and adds them to list
	+ prints them out in console
*/

import java.util.Scanner;
import java.util.*;

public class Main {
	public static void main(String[] args) {
		System.out.println("Enter number of names: "); 
		Scanner number = new Scanner(System.in);
		int a = number.nextInt();	//takes as input first: number of names
		ArrayList<String> list=new ArrayList<String>();
		for (int i = 0; i < a; i++) {
			System.out.println("Enter " + ordinal(i+1) + " name: ");
			Scanner input = new Scanner(System.in);
			list.add(input.nextLine());	//than reads these names and adds them to list
		}
		System.out.println(list);	//prints them out in console
	}
	//method getting ordinal of a number
	public static String ordinal(int i) {
		String[] sufixes = new String[] {"th", "st", "nd", "rd", "th", "th", "th", "th", "th", "th"};
		switch (i % 100) {
		case 11: case 12: case 13:
			return i + "th";
		default:
			return i + sufixes[i % 10];
		}
	}
}
